# Introdução
Documentação da aula sobre Laços de Repetição passada na semana 1 no dia 01/10/2021.
<br>

## [Repetição](./REPETICAO.md#repeti%C3%A7%C3%A3o)
### [O que é](./REPETICAO.md#o-que-%C3%A9)
### [O que pode fazer ](./REPETICAO.md#o-que-pode-fazer)
### [Tipos de estrutura](./REPETICAO.md#tipos-de-estrutura)
#### [1. Repetição com teste no início](./REPETICAO.md#1-while-teste-no-início)
#### [2. Repetição com teste no final](./REPETICAO.md#2-do-while-teste-no-final)
#### [3. Repetição com teste controlado](./REPETICAO.md#3-for-repeti%C3%A7%C3%A3o-controlada)

### [Exercício](./numeroPensado)