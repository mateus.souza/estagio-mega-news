# Número Pensado
Imagine duas pessoas brincando de advinhação, uma delas pensa num número e a outra tem que advinhar. <br>
Transfira esse jogo para código. <br>
Seu código deve:
- Receber o número pensado
- Receber um número indeterminado de chutes caso não estejam corretos
- Imprimir o chute correto
- Imprimir a quantidade de tentativas
