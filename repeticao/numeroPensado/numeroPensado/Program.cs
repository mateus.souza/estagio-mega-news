﻿using System;

namespace numeroPensado
{
    class Program
    {
        static void Main(string[] args)
        {
            int pensamento;
            int chute;
            int tentativas = 0;
            
            Console.WriteLine("Digite o número pensado: ");
            pensamento = Convert.ToInt16(Console.ReadLine());
            
            do
            {
                Console.WriteLine("Chute: ");
                chute = Convert.ToInt16(Console.ReadLine());
                tentativas++;
            } while (chute != pensamento);
            Console.WriteLine("Acertou!");
            Console.WriteLine("Chute correto: " + chute);
            Console.WriteLine("Quantidade de tentativas: " + tentativas);
        }
    }
}