# Repetição
## O que é:
Repetição, laço de repetição ou loop, é um tipo de estrutura de código que permite a repetição de uma ação mediante a uma determinada condição até que ela se torne verdadeira, falsa ou por uma determinada quantidade de vezes.

## O que pode fazer:
- Efetuar uma ação múltiplas vezes;
- Evitar repetição de código;
- Abstrair quantidades indeterminadas de interação;
- Criar bugs que congelam o sistema.


## Tipos de estrutura:
Existem 3 tipos de estrutura de repetição. São eles: <br>
1. [Repetição com teste no início](./REPETICAO.md#1-while-teste-no-início) <br>
1. [Repetição com teste no final](./REPETICAO.md#2-do-while-teste-no-final)<br>
1. [Repetição com teste controlado](./REPETICAO.md#3-for-repeti%C3%A7%C3%A3o-controlada)

<br>

### **1. While - Teste no Início**
Na estrutura de repetição com teste no início, denominada "while" na linguagem C#, o parâmetro de quebra do laço de repetição é declarado após o nome da função.
### Estrutura:

~~~
enquanto <condição> repita
    C1;
    C2;
    ...
    Cn;
fimenquanto;

//Enquanto a condição for verdadeira os comandos dentro do escopo serão executados. 
//Quando a condição se torna falsa o laço de repetição é quebrado.
~~~

### Exemplo de código em C#:
~~~ cs
int contador = 0;
while (contador < 10)
{
    contador++;
    Console.WriteLine(contador);
}
//enquanto a variável 'contador' for menor que 10 a variável será incrementada e será impresso o valor atual dela
//repetindo esse processo até que a variável não seja mais menor que 10
~~~
![Diagrama1](./materiais/diagrama1.png)

<br>

### **2. Do while - Teste no Final**
Na estrutura de repetição com teste no final, denominada "do while" no C#, os comandos são dados primeiro e a verificação da condição apenas no final.

### Estrutura:
~~~
repita
    C1;
    C2;
    ...
    Cn;
    enquanto <condição>
~~~
### Exemplo de código em C#:
~~~ cs
int numero;
bool impar;
do
{
    Console.WriteLine("Digite um número ímpar: ");
    numero = Convert.ToInt16(Console.ReadLine());
    if (numero % 2 != 0)
    {
        impar = true;
        Console.WriteLine(numero + " é um número ímpar. Obrigado!");
    }
    else
    {
        impar = false;
        Console.WriteLine(numero + " não é um número ímpar. Tente novamente!");
    }
} while (impar != true);

//o código pede um número, testa se ele é impar e atribui um valor Verdadeiro ou Falso para a variável 'impar'. 
//Enquanto o valor da variável não for Verdadeiro o código continua.
~~~

![Diagrama2](./materiais/diagrama2.png)

<br>

### **3. For - Repetição Controlada**
Na estrutura de repetição controlada, denominada "for" no C#, a repetição não ocorre sobre uma condição booleana atribuída como as demais estruturas, e sim sobre a quantidade de repetições delimitada por um array seguindo a fórmula abaixo: <br>
`Q = ((vf-vi) div p) + 1`

Q = Quantidade de repetições <br>
vf = valor final <br>
vi = valor inicial <br>
p = passo (quantos elementos pula de cada vez)
### Estrutura:
~~~
para V de vi até vf passo p faça
    C1;
    C2;
    ...
    Cn;
fimpara;
~~~
### Exemplo de código em C#:
~~~ cs
int cont = 0;
int repete;

Console.WriteLine("Quantidade de repetições: ");
repete = Convert.ToInt16(Console.ReadLine());

for (int i = 0; i < repete i++)
            {
                cont++;
                Console.WriteLine("Repetição ")
                Console.Write(cont);                
            }

//o código pede a quantidade de repetições e executa o código pela quantidade de vezes pedida

~~~

<img src="./materiais/diagrama3.png" alt="Diagrama 3" width="600"/>

<br>

## Referências:
Livro [Lógica de Programação 3ª Edição - Forbellone e Eberspacher](https://gitlab.com/GomessGuii/estagio/-/blob/main/Logica_de_Programacao_3_Edicao_Livro.pdf)

Este documento pode ser encontrado publicamente em: [https://gitlab.com/GomessGuii/estagio/](https://gitlab.com/GomessGuii/estagio/-/tree/main/selecao)