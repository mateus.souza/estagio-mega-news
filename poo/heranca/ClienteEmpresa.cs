using System;

namespace poo
{
    class ClienteEmpresa : Pessoa
    {
        //Atributos
        private int codigoCliente;
        private double mensalidadeCliente;
        private int anoContrato;

        //Construtor
        public ClienteEmpresa(int codigoCliente, string nome, int anoNascimento, double mensalidadeCliente,
            int anoContrato)
        {
            this.codigoCliente = codigoCliente;
            this.nome = nome;
            this.anoNascimento = anoNascimento;
            this.mensalidadeCliente = mensalidadeCliente;
            this.anoContrato = anoContrato;

            dadosPessoa();
            dadosClienteEmpresa();

        }

        //Metodo de receber dados de Cliente da empresa
        private void dadosClienteEmpresa()
        {
            Console.WriteLine("Codigo: " + codigoCliente);
            Console.WriteLine("Mensalidade: " + mensalidadeCliente);
            Console.WriteLine("Ano contrato: " + anoContrato);
        }

        
    }
}