﻿using System;
using System.Reflection.Metadata;

namespace poo
{
     public class Program
    {
        
        static void Main(string[] args)
        {
            Console.WriteLine("Herança de colaborador: ");
            var obj = new ColaboradorEmpresa("Mateus", 2000, 1045);
           
            
            Console.WriteLine();
            Console.WriteLine("Herança de cliente: ");
            
            ClienteEmpresa obj1 = new ClienteEmpresa(1, "Eduardo", 1995, 200, 2021);

            if (obj is Pessoa)
            {
                Console.WriteLine("de fato isso e uma pessao");
            }
        }
    }
}
