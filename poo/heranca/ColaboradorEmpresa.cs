using System;
namespace poo
{
    class ColaboradorEmpresa : Pessoa
    {
        //Atributos
        private double salario;
        
        //Construtor 
        public ColaboradorEmpresa(string nome, int anoNascimento, double salario)
        {
            this.nome = nome;
            this.anoNascimento = anoNascimento;
            this.salario = salario;
            
            dadosPessoa();
            dadosColaborador();
        }
        
        //Metodo de receber dados de Colaborador
        private void dadosColaborador()
        {
            Console.WriteLine("Salario: " +salario);
        }

        
    }
}