using System.ComponentModel;
using System;

namespace interface1
{
    public class Circulo : Cartesiano, Iforma, inter
    {
        //Criando construtor
        private double raio;
        private Circulo(int x, int y, double raio)
        {
            this.x = x;
            this.y = y;
            this.raio = raio;
        }
        
        // public void desenhar()
        // {
        //     //DESENHANDO O CIRCULO EM X E Y
        //     Console.WriteLine("Circulo: " +x, +y, +raio);
        // }
        public void desenhar()
        {
           
            Console.WriteLine("Circulo: " +x+"," +y+"," +raio);
        }

        public static Cartesiano CriarCartesiano(int x, int y, double raio)
        {
            return new Circulo(x,y,raio);
        }

        public static Iforma CriarForma(int x, int y, double raio)
        {
            return new Circulo(x,y,raio);
        }


        public void interrr()
        {
            throw new NotImplementedException();
        }
    }
}