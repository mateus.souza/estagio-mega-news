﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace interface1
{
    class Program
    {
        public static void Main(string[] args)
        {
            List<Iforma> l = new List<Iforma>();
            l.Add(Retangulo.criarCartesianoForma(10,12,22,34));
            l.Add(Circulo.CriarForma(1,2,22.0));
            
          
           l.ForEach(forma => forma.desenhar());
           
           l.Select(forma => forma is Cartesiano)
               .ToImmutableList()
               .ForEach(Console.WriteLine);
           
        }
    }
}
