using System;

namespace interface1
{
    public class Retangulo : Cartesiano, Iforma
    {
        private double altura;
        private double largura;

        private Retangulo(int x, int y, double altura, double largura)
        {
            this.x = x;
            this.y = y;
            this.altura = altura;
            this.largura = largura;
        }


        public void desenhar()
        {
            
            //desenha o retangulo em x e y
            Console.WriteLine("Retangulo: " +x+"," +y+"," +altura+"," +largura);
        }

        public static Cartesiano criarCartesiano(int x, int y, double altura, double largura)
        {
            return new Retangulo(x,y,altura,largura);
        }

        public static Iforma criarCartesianoForma(int x, int y, double altura, double largura)
        {
            return new Retangulo(x, y, altura, largura);
        }
    }
}