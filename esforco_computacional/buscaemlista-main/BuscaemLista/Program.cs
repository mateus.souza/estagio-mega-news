using System;
using System.Collections;
using System.Linq;

namespace BuscaemLista
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] guardaLista = new int[100];
            Random aleatorio = new Random();
            int tamanhoLista = aleatorio.Next(100);
            for (int i = 0; i <= tamanhoLista; i++)
            {
                guardaLista[i] = aleatorio.Next(1000);
            }
            
            // for (int j = 0; j <= tamanhoLista; j++)
            // {
            //     Console.Write(guardaLista[j] + " ");
            // }

            int comprimento = tamanhoLista + 1;
            Console.WriteLine("Tamanho: " + comprimento);
            int numeroPensado= Convert.ToInt16(Console.ReadLine());
            
            int posicao=0;
            int cont=1;
            
            while (numeroPensado != guardaLista[posicao] && posicao != tamanhoLista)
            {
                cont++;
                posicao++;
            }
            
            if (guardaLista[posicao] != numeroPensado)
            {
                posicao = -1;
            }
            
            Console.WriteLine("Posição: " + posicao);
            Console.WriteLine("Esforço: " + cont);
        }
    }
}
