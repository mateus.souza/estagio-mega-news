# Introdução
Documentação da aula sobre Estruturas de Seleção passada na semana 1 no dia 30/09/2021.
<br>

# Índice
## [Seleção](./SELECAO.md#sele%C3%A7%C3%A3o)
### [O que é](./SELECAO.md#o-que-%C3%A9)
### [O que pode fazer ](./SELECAO.md#o-que-pode-fazer)
### [Tipos de estrutura](./SELECAO.md#tipos-de-estrutura)
#### [1. Seleção de um ramo](./SELECAO.md#1-sele%C3%A7%C3%A3o-de-um-ramo)
#### [2. Seleção de dois ramos](./SELECAO.md#2-sele%C3%A7%C3%A3o-de-dois-ramos)
#### [3. Seleção múltipla](./SELECAO.md#3-sele%C3%A7%C3%A3o-m%C3%BAltipla)

### [Exercício](./numeroPensado)
