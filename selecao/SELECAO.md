# Seleção
## O que é:
Estruturas de seleção como o próprio nome diz são estruturas utilizadas para selecionar alguma coisa. <br>

## O que pode fazer:
- Alterar variáveis mediante escolha do usuário;
- Alterar o caminho que o código segue;
- Atribuir estruturas condicionais.

## Tipos de Estrutura: 

[1. Seleção de um ramo](./SELECAO.md#1-sele%C3%A7%C3%A3o-de-um-ramo) <br>
[2. Seleção de dois ramos](./SELECAO.md#2-sele%C3%A7%C3%A3o-de-dois-ramos) <br>
[3. Seleção múltipla](./SELECAO.md#3-sele%C3%A7%C3%A3o-m%C3%BAltipla)

<br>

## 1. Seleção de um ramo
Uma estrutura condicional de um ramo define uma condição que ,se for verdadeira, executa os comandos definidos e, se não, não os executa.
### Estrutura:
~~~
se <condição> então:
    C1;
    C2;
    ...
    Cn;
fimse;

//se a condição for falsa os comandos são ignorados
~~~
### Exemplo de código em C#:
~~~ cs
Console.WriteLine("Qual o seu nome?");
string nome = Console.ReadLine();

if (nome == "Guilherme")
{
    Console.WriteLine("Que nome bonito!");
}    

Console.WriteLine("Seja bem-vindo(a), " + nome + "!");
//caso o nome seja "Guilherme" é impressa a mensagem "Que nome bonito!"
//se o nome for qualquer outra coisa a mensagem não é impressa
~~~

<br>

## 2. Seleção de dois ramos
Numa estrutura de seleção de dois ramos são definidos dois caminhos a serem seguidos, um caso a condição seja Verdadeira e outro caso seja Falsa.
### Estrutura:
~~~
se <condição> então:
    C1;
    C2;
    ...
    Cn;
senão então:
    D1;
    D2;
    ...
    Dn;
fimse;
~~~
### Exemplo de código em C#:
~~~ cs
Console.WriteLine("Qual o seu nome?");
string nome = Console.ReadLine();

if (nome == "Guilherme")
{
    Console.WriteLine("Que nome bonito!");
}
else 
{
    Console.WriteLine("Que nome legal!")
}    

Console.WriteLine("Seja bem-vindo(a), " + nome + "!");
//este exemplo segue o mesmo padrão do anterior exceto que se o nome for diferente de "Guilherme" é apresentada 
//uma mensagem diferente em vez de simplesmente continuar
~~~

<br>

## 3. Seleção múltipla
De forma semelhante ao anterior, a seleção múltipla permite mais de um caminho a ser seguido, a diferença é que a quantidade de caminhos é praticamente infinita na seleção múltipla.
### Estrutura:
~~~
se <condição1> então:
    C1;
    C2;
    ...
    Cn;
senão se <condição2> então:
    D1;
    D2;
    ...
    Dn;
senão se <condição3> então:
    E1;
    E2;
    ...
    En;
senão então:
    F1;
    F2;
    ...
    Fn;
fimse;
//note que mesmo havendo inúmeros "senão se" ainda pode ser atribuído um "senão" ao final
~~~
### Exemplo de código em C#:
~~~ cs
Console.WriteLine("Qual o seu nome?");
string nome = Console.ReadLine();

if (nome == "Guilherme")
{
    Console.WriteLine("Que nome bonito!");
}
else if (nome == "Xekira")
{
    Console.WriteLine("Que nome interessante!")
} 
else
{
    Console.WriteLine("Que nome legal!")
}

Console.WriteLine("Seja bem-vindo(a), " + nome + "!");

//neste outro exemplo já foi adicionada uma terceira opção de mensagem
~~~
## Referências:
Livro: [Lógica de Programação 3ª Edição - Forbellone e Eberspacher](https://gitlab.com/GomessGuii/estagio/-/blob/main/Logica_de_Programacao_3_Edicao_Livro.pdf)

Este documento pode ser encontrado publicamente em: [https://gitlab.com/GomessGuii/estagio/](https://gitlab.com/GomessGuii/estagio/-/tree/main/selecao)